<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Featured extends Model
{
	protected $table = 'featured';

	protected $fillable = [
        'user_id'
    ];

    public function user(){
		return $this->belongsTo('App\Models\User');
	}
}
