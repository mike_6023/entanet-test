<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'surname', 'dob', 'phone_number', 'approved', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

	//create mutator to always conver the price to pence
	public function setPasswordAttribute($password){
		$this->attributes['password'] = Hash::make($password);
	}

	public function roles(){
		return $this->belongsToMany('App\Models\Role')->withTimestamps();
	}

	public function trades(){
		return $this->belongsToMany('App\Models\Trade')->withTimestamps();
	}

	public function skills(){
		return $this->belongsToMany('App\Models\Skill')->withTimestamps();
	}

	public function appointments(){
		return $this->hasMany('App\Models\Appointment');
	}

	public function featured(){
		return $this->hasOne('App\Models\Featured');
	}

	public function isAdmin() {
	   return $this->roles()->where('name', 'admin')->exists();
	}

	public function isTradesperson() {
	   return $this->roles()->where('name', 'tradesperson')->exists();
	}

	public function isUser() {
	   return $this->roles()->where('name', 'user')->exists();
	}
}
