<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\RepositoryInterface;
use App\Abstracts\Repository;
use Auth;

class FeaturedRepository extends Repository
{
    // Constructor to bind model to repo
    public function __construct(){
        $this->model = new \App\Models\Featured;
    }
}
