<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\RepositoryInterface;
use App\Abstracts\Repository;
use Auth;

class TradeRepository extends Repository
{
    // Constructor to bind model to repo
    public function __construct()
    {
        $this->model = new \App\Models\Trade;
    }

	public function searchUsersByTrade($trade)
	{
		return $this->with(['users' => function($query){
			$query->where('approved', 1);
			$query->whereDoesntHave('featured');
		},'users.skills'])->where('id', $trade)->first();
	}

	public function getFeaturedUsersByTrade($trade){
		return $this->model->with(['users' => function($query){
			$query->where('approved', 1);
			$query->has('featured');
		}, 'users.skills', 'users.trades'])->where('id', $trade)->first();
	}
}
