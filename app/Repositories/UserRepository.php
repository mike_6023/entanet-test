<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\RepositoryInterface;
use App\Abstracts\Repository;
use Auth;

class UserRepository extends Repository
{
    // Constructor to bind model to repo
    public function __construct()
    {
        $this->model = new \App\Models\User;
    }

	// Get all instances of model
    public function allTradesPeople()
    {
        return $this->model->with('featured')->has('trades')->has('skills')->paginate(16);
    }

	// Get all instances of model
    public function allUsers()
    {
        return $this->model->with('roles')->whereHas('roles', function($query){
			$query->where('name', 'user');
		})->get();
    }

	public function create(array $data)
    {
        $user = $this->model->create($data);
		$role = \App\Models\Role::where('name', 'user')->pluck('id');
		$user->roles()->attach($role);
    }

	public function getTradespersonById($id){
		return $this->model->with('trades', 'skills')->find($id);
	}

    public function approve($id)
    {
        $user = $this->model->find($id);
		$user->approved = 1;

		$user->save();

    }

	public function blacklist($id)
    {
        $user = $this->model->find($id);
		$user->approved = 0;

		$user->save();

    }

	public function delete($id){
		$user = $this->model->find($id);
		$user->delete();
		$user->roles()->detach();
	}

    //return if the current user has the admin role assigned
	public function isAdmin(){
		return \Auth::User()->isAdmin();
	}

	//return if the current user has the admin role assigned
	public function isTradesperson(){
		return \Auth::User()->isTradesperson();
	}

	//return if the current user has the admin role assigned
	public function isUser(){
		return \Auth::User()->isUser();
	}
}
