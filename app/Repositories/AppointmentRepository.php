<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\RepositoryInterface;
use App\Abstracts\Repository;
use Auth;

class AppointmentRepository extends Repository
{
    // Constructor to bind model to repo
    public function __construct()
    {
        $this->model = new \App\Models\Appointment;
    }

	public function create(array $data)
    {
        $this->model->requested_by = Auth::User()->id;
		$this->model->appointment_with = $data['user_id'];
		$this->model->notes = $data['notes'];

		return $this->model->save();
    }
}
