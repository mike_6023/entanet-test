<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
            'firstname' => 'required|string|max:255',
			'surname' => 'required|string|max:255',
			'email' => 'required|email|unique:users|max:255',
			'phone_number' => 'sometimes|nullable|numeric',
			'dob' => 'sometimes|nullable|date_format:Y-m-d',
			'password' => ['required', 'string', 'min:8', 'confirmed']
        ];
    }

	public function messages(){
		return [
			'dob.date_format' => 'DOB must match the following format: yyyy-mm-dd',
		];
	}
}
