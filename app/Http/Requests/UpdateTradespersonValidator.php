<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTradespersonValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string|max:255',
			'surname' => 'required|string|max:255',
			'email' => 'required|email|max:255',
			'trades' => 'required',
			'skills' => 'required',
			'phone_number' => 'sometimes|nullable|numeric',
			'dob' => 'sometimes|nullable|date_format:Y-m-d'
        ];
    }

	public function messages(){
		return [
			'dob.date_format' => 'DOB must match the following format: yyyy-mm-dd',
		];
	}
}
