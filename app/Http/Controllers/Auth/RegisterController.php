<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 protected $tradeRepo;
	 protected $skillRepo;

    public function __construct(
		\App\Repositories\TradeRepository $tradeRepo,
		\App\Repositories\SkillRepository $skillRepo
	){
        $this->middleware('guest');
		$this->tradeRepo = $tradeRepo;
		$this->skillRepo = $skillRepo;
    }

	public function showRegistrationForm()
	{
		$trades = $this->tradeRepo->all();
		$skills = $this->skillRepo->all();
		//dd($trades);
	    return view("auth.register", compact('trades', 'skills'));
	}

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
			'surname' => ['required', 'string', 'max:255'],
			'trades' => ['required', 'max:255'],
			'skills' => ['required', 'max:255'],
			'dob' => ['required', 'sometimes', 'nullable', 'string', 'max:255'],
			'telephone' => ['required', 'sometimes', 'nullable', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
		try{
	        $user = \App\Models\User::create([
				'firstname' => $data['firstname'],
				'surname' => $data['surname'],
	 			'email' => $data['email'],
				'dob' => ((isset($data['dob']))? $data['dob'] : NULL),
				'telephone' => ((isset($data['telephone']))? $data['telephone'] : NULL),
	            'password' => Hash::make($data['password']),
	        ]);

			$role = Role::where('name', 'tradesperson')->first();
			$user->roles()->attach($role->id);
			$user->trades()->attach($data['trades']);
			$user->skills()->attach($data['skills']);

			return $user;
		}catch(Exception $e){
			Log::error($e->getMessage());
			return redirect()->back()->withError('Error, Please contact support');
		}
    }
}
