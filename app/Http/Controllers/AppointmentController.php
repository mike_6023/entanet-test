<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AppointmentValidator;
use App\Repositories\AppointmentRepository;

class AppointmentController extends Controller
{
	private $appointmentRepo;

	public function __construct(AppointmentRepository $appointmentRepo){
		$this->appointmentRepo = $appointmentRepo;
	}

    public function store(AppointmentValidator $request){
		$this->appointmentRepo->create($request->all());

		return redirect()->back()->withSuccess('Thank You, your appointment has been requested');
	}
}
