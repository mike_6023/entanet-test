<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TradeRepository;
use App\Repositories\FeaturedRepository;

class SearchController extends Controller
{
	private $tradeRepo;
	private $featuredRepo;

	public function __construct(TradeRepository $tradeRepo){
		$this->tradeRepo = $tradeRepo;
	}

    public function index(Request $request){
		$results = [];
		$featured = [];
		if(isset($request->trade_cat)){
			$featured = $this->tradeRepo->getFeaturedUsersByTrade($request->trade_cat);
			$results = $this->tradeRepo->searchUsersByTrade($request->trade_cat);
		}
		//print_r($featured);die;
		return view('search.index', compact('results', 'featured'));
	}
}
