<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \App\Repositories\UserRepository;
use \App\Http\Controllers\Controller;

class ApproveController extends Controller
{
	private $userRepo;

    public function __construct(UserRepository $userRepo){
		$this->userRepo = $userRepo;
	}

	public function update(Request $request){
		$user = $this->userRepo->approve($request->id);

		return redirect()->back();
	}
}
