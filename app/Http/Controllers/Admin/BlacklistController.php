<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class BlacklistController extends Controller
{
	private $userRepo;

    public function __construct(UserRepository $userRepo){
		$this->userRepo = $userRepo;
	}

	public function update(Request $request){
		$user = $this->userRepo->blacklist($request->id);

		return redirect()->back();
	}
}
