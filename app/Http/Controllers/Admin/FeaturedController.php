<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\FeaturedRepository;

class FeaturedController extends Controller
{
	private $featuredRepo;

	public function __construct(FeaturedRepository $featuredRepo){
		$this->featuredRepo = $featuredRepo;
	}

    public function create(Request $request){
		$this->featuredRepo->create($request->all());

		return redirect()->back()->withSuccess('This user has been set as featured');
	}
}
