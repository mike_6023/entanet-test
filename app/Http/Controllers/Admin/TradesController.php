<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use \App\Repositories\UserRepository;
use \App\Repositories\TradeRepository;
use \App\Repositories\SkillRepository;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateTradespersonValidator;

class TradesController extends Controller
{
	private $userRepo;
	private $tradeRepo;
	private $skillRepo;

	public function __construct(UserRepository $userRepo, TradeRepository $tradeRepo, SkillRepository $skillRepo){
		$this->userRepo = $userRepo;
		$this->tradeRepo = $tradeRepo;
		$this->skillRepo = $skillRepo;
	}

    public function index(){
		$tradespeople = $this->userRepo->allTradesPeople();

		return view('admin.trades.index', compact('tradespeople'));
	}

	public function edit($id){
		$tradesperson = $this->userRepo->getTradespersonById($id);
		$trades = $this->tradeRepo->all();
		$skills = $this->skillRepo->all();

		return view('admin.trades.edit', compact('tradesperson', 'trades', 'skills'));
	}

	public function update(UpdateTradespersonValidator $request, $id){
		try{
			$tradesperson = $this->userRepo->update($request->all(), $id);
			$tradesperson->skills()->sync($request->skills);
			$tradesperson->trades()->sync($request->trades);

			return redirect()->back()->withSuccess('Updated Successfully');
		}catch(Exception $e){
			Log::error($e->getMessage());

			return redirect()->back()->with('error', 'There has been an error, please contact support');
		}
	}
}
