<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserValidator;
use App\Repositories\UserRepository;

class UserController extends Controller
{
	public function __construct(UserRepository $userRepo){
		$this->userRepo = $userRepo;
	}

    public function index(){
		$users = $this->userRepo->allUsers();

		return view('admin.users.index', compact('users'));
	}

	public function create(){
		return view('admin.users.create');
	}

	public function store(CreateUserValidator $request){
		try{
			$this->userRepo->create($request->all());

			return redirect(route('users.all'))->withSuccess('User created successfully');
		}catch(\Exception $e){
			Log::error($e->getMessage());
			return redirect()->back()->withError('There has been an error, please contact support');
		}
	}

	public function delete($id){
		try{
			$this->userRepo->delete($id);
			return redirect(route('users.all'))->withSuccess('User Deleted successfully');
		}catch(\Exception $e){
			Log::error($e->getMessage());
			return redirect()->back()->withError('There has been an error, please contact support');
		}
	}
}
