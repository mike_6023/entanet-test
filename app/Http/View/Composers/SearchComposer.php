<?php

namespace App\Http\View\Composers;

use App\Repositories\TradeRepository;
use Illuminate\View\View;

class SearchComposer
{
    /**
     * The trade repository implementation.
     *
     * @var TradeRepository
     */
    protected $trades;

    /**
     * Create a new search composer.
     *
     * @param  TradeRepository  $trades
     * @return void
     */
    public function __construct(TradeRepository $trades)
    {
        $this->trades = $trades;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('trades', $this->trades->all());
    }
}
