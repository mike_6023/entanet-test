<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use \App\Repositories\UserRepository;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$user = new UserRepository;

		if($user->isUser() || $user->isAdmin()){
			return $next($request);
		}

		return redirect()->back();
    }
}
