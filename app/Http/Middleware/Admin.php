<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use \App\Repositories\UserRepository;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$user = new UserRepository;

		if($user->isAdmin()){
			return $next($request);
		}
		
		return redirect('/');
    }
}
