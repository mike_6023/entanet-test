<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function(){
	return view('welcome');
});
Route::get('/search', 'SearchController@index')->name('search.index');
//only allow admin to view all trades
Route::namespace('Admin')->middleware(['auth', 'admin'])->prefix('admin')->group(function(){
	Route::patch('approve', 'ApproveController@update')->name('user.approve');
	Route::post('featured', 'FeaturedController@create')->name('featured.create');
	Route::patch('blacklist', 'BlackListController@update')->name('user.blacklist');

	Route::prefix('tradespeople')->group(function(){
		Route::get('/', 'TradesController@index')->name('trades.all');
		Route::get('/edit/{id}', 'TradesController@edit')->name('trades.edit');
		Route::patch('/edit/{id}/update', 'TradesController@update')->name('trades.update');
	});

	Route::prefix('user')->group(function(){
		Route::get('/', 'UserController@index')->name('users.all');
		Route::get('/create', 'UserController@create')->name('users.create');
		Route::post('/store', 'UserController@store')->name('users.store');
		Route::delete('/disable/{id}', 'UserController@delete')->name('users.delete');
	});
});
//only allow admin and tradesperson to edit tradesperson profile
Route::namespace('Tradesperson')->middleware(['auth', 'tradesperson'])->prefix('admin')->group(function(){
	Route::get('profile/{id}', 'ProfileController@show')->name('profile.show');
});
//only allow a user to book an appointment
Route::middleware(['auth', 'user'])->group(function(){
	Route::post('/book-appointment', 'AppointmentController@store')->name('appointment.store');
});
