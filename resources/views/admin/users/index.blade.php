@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card">
				<div class="card-header">
					User Admin
					<a style="float:right;" class="btn btn-success" href="{{ route('users.create') }}">Create</a>
				</div>

				<div class="card-body">
					@include('partials.errors')
					@include('partials.success')

					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">Firstname</th>
								<th scope="col">Surname</th>
								<th scope="col">Email</th>
								<th scope="col">Disable</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users AS $user)
								<tr>
									<td>{{ $user->firstname }}</td>
									<td>{{ $user->surname }}</td>
									<td>{{ $user->email }}</td>
									<td>
										<form action="{{ route('users.delete', ['id' => $user->id]) }}" name="delete_user" method="POST">
											@csrf
											@method('DELETE')
											<input type="hidden" name="id" value="{{ $user->id }}">
											<button class="btn btn-danger">Disable</button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
