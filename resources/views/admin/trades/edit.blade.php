@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit') }}</div>
                <div class="card-body">
					@include('partials.errors')
					@include('partials.success')
                    <form method="POST" action="{{ route('trades.update', ['id' => $tradesperson->id]) }}">
                        @csrf
						@method('PATCH')
                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname', $tradesperson->firstname) }}" required autocomplete="firstname" autofocus>

                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname', $tradesperson->surname) }}" required autocomplete="surname" autofocus>

                                @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $tradesperson->email) }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="trades" class="col-md-4 col-form-label text-md-right">{{ __('Trades') }}</label>

                            <div class="col-md-6">
                                <select id="trades" name="trades[]" multiple required>
									@foreach($trades AS $trade)
										@foreach($tradesperson->trades AS $selected_trade)
											<option @if($trade->id == $selected_trade->id) selected @endif value="{{ $trade->id }}">{{ $trade->name }}</option>
										@endforeach
									@endforeach
								</select>
                                @error('trades')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $trades }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="skills" class="col-md-4 col-form-label text-md-right">{{ __('Skills') }}</label>

                            <div class="col-md-6">
                                <select id="skills" name="skills[]" multiple required>
									@foreach($skills AS $skill)
										@foreach($tradesperson->skills AS $selected_skill)
											<option @if($skill->id == $selected_skill->id) selected @endif value="{{ $skill->id }}">{{ $skill->name }}</option>
										@endforeach
									@endforeach
								</select>
                                @error('skills')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $skills }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Telephone') }}</label>

							<div class="col-md-6">
                                <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number', $tradesperson->phone_number) }}" autocomplete="phone_number">

                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<div class="form-group row">
							<label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date Of Birth') }}</label>

							<div class="col-md-6">
								<input id="dob" type="text" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob', $tradesperson->dob ) }}" autocomplete="dob" autofocus>

								@error('dob')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
