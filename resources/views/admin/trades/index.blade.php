@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10">
			<div class="card">
				<div class="card-header">Trades People</div>

				<div class="card-body">
					@include('partials.errors')
					@include('partials.success')

					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">Firstname</th>
								<th scope="col">Surname</th>
								<th scope="col">Email</th>
								<th scope="col">Trades</th>
								<th scope="col">Approve/Blacklist</th>
								<th scope="col">Edit</th>
								<th scope="col">Featured</th>
							</tr>
						</thead>
						<tbody>
							@foreach($tradespeople AS $tradesperson)
								<tr>
									<td>{{ $tradesperson->firstname }}</td>
									<td>{{ $tradesperson->surname }}</td>
									<td>{{ $tradesperson->email }}</td>
									<td>
										@foreach($tradesperson->trades AS $trade)
											{{ $trade->name }}
										@endforeach
									</td>
									<td>
										@if($tradesperson->approved == 0)
											<form action="{{ route('user.approve') }}" name="approve" method="POST">
												@csrf
												@method('PATCH')
												<input type="hidden" name="id" value="{{ $tradesperson->id }}">
												<button class="btn btn-primary">Approve</button>
											</form>
										@else
											<form action="{{ route('user.blacklist') }}" name="blacklist" method="POST">
												@csrf
												@method('PATCH')
												<input type="hidden" name="id" value="{{ $tradesperson->id }}">
												<button class="btn btn-danger">Black List</button>
											</form>
										@endif
									</td>
									<td><a href="{{ route('trades.edit', ['id' => $tradesperson->id ]) }}" class="btn btn-success">Edit</a></td>
									<td>
										@if(!isset($tradesperson->featured))
											<form action="{{ route('featured.create') }}" name="featured" method="POST">
												@csrf
												<input type="hidden" name="user_id" value="{{ $tradesperson->id }}">
												<button class="btn btn-primary">Featured</button>
											</form>
										@else
											Featured
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{{ $tradespeople->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
