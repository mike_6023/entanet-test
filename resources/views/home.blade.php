@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(\Auth::User()->isAdmin())
						<a href="{{ route('trades.all') }}">Trades Admin</a><br />
						<a href="{{ route('users.all') }}">User Admin</a><br />
						<a href="{{ route('search.index') }}">Search Tradespeople</a>
					@elseif(\Auth::User()->isTradesperson())
						<a href="{{ route('profile.show', ['id' => \Auth::User()->id]) }}">View Profile</a>
					@else
						<a href="{{ route('search.index') }}">Search Tradespeople</a>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
