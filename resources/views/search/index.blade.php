@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Search</div>

				<div class="card-body">
					@include('search.form')
					@include('partials.errors')
					@include('partials.success')
					@if(!empty($results) || !empty($featured))
						@if(($results->users->count() == 0) && ($featured->users->count() == 0))
							There are no people registered with that trade, please try again later
						@else
						<div class="row">
							@foreach($featured->users AS $feature)
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="card mt-2">
										<div class="card-header">
											Featured Listing
										</div>
										<div class="card-body">
											<h3 class="card-title">{{ $feature->firstname }} {{ $feature->surname }}</h3>
											<h4>Skill List</h4>
											<p class="card-text">
												@foreach($feature->skills AS $skill)
													{{ $skill->name }}
												@endforeach
											</p>
											@if(Auth::check() && Auth::User()->isUser())
												<!-- Button trigger modal -->
												<button data-id="{{ $feature->id }}" type="button" class="btn btn-primary make-appointment" data-toggle="modal" data-target="#exampleModal">
												  	Make Appointment
												</button>
											@endif
										</div>
									</div>
								</div>
							@endforeach
							@foreach($results->users AS $tradesperson)
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="card mt-2">
										<div class="card-header">
											{{ $results->name }}
										</div>
										<div class="card-body">
											<h3 class="card-title">{{ $tradesperson->firstname }} {{ $tradesperson->surname }}</h3>
											<h4>Skill List</h4>
											<p class="card-text">
												@foreach($tradesperson->skills AS $skill)
												{{ $skill->name }}
												@endforeach
											</p>
											@if(Auth::check() && Auth::User()->isUser())
												<!-- Button trigger modal -->
												<button data-id="{{ $tradesperson->id }}" type="button" class="btn btn-primary make-appointment" data-toggle="modal" data-target="#exampleModal">
												  	Make Appointment
												</button>
											@endif
										</div>
									</div>
								</div>
							@endforeach
						</div>
						@endif
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@if(Auth::check() && Auth::User()->isUser())
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Make Appointment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="{{ route('appointment.store') }}" name="make-appointment" method="POST">
					@csrf
					<div class="modal-body">
						<input id="user_id" type="hidden" name="user_id" value="">
						<textarea class="form-control" name="notes" placeholder="Please specify what your appointment is regarding"></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button class="btn btn-primary">Make Appointment</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endif
@endsection
