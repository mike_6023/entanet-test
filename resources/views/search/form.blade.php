<form name="search" action="{{ route('search.index') }}" method="GET">
	<div class="form-row align-items-center">
		<div class="col-sm-10 my-1">
			<select class="form-control" id="search" name="trade_cat" required>
				@foreach($trades AS $trade)
					<option @if(app('request')->input('trade_cat') == $trade->id) selected @endif value="{{ $trade->id }}">{{ $trade->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-auto my-1">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</form>
