<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('skills')->insert([
			['name' => 'Domestic Electrics'],
			['name' => 'Industrial Electrics'],
			['name' => 'Hand Made Stairs'],
			['name' => 'Carpet Fitter'],
			['name' => 'Fork Lift Specialist'],
			['name' => 'Cavity Insulation'],
			['name' => 'Automotive Painter'],
			['name' => 'Gas Engineer'],
		]);
    }
}
