<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
		$role = App\Models\Role::where('name', 'admin')->pluck('id');

		$admin = \App\Models\User::create([
			'firstname' => 'Mike',
			'surname' => 'Adams',
			'email' => 'mikeadams1988@gmail.com',
			'password' => Hash::make('test123')
		]);
		$admin->roles()->attach($role);


		factory(\App\Models\User::class, 50)->create()->each(function ($user) {
			$role = App\Models\Role::inRandomOrder()->first();
			//assign a trade to a user if the role pulled back is a tradesperson
			if($role->name == 'tradesperson'){
				$user->trades()->attach(App\Models\Trade::inRandomOrder()->first());
				$user->skills()->attach(App\Models\Skill::inRandomOrder()->first());
				$user->roles()->attach($role->id);
			}else{
	        	$user->roles()->save(App\Models\Role::inRandomOrder()->first('id'));
			}
	    });
    }
}
