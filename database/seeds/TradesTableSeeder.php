<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('trades')->insert([
			['name' => 'Tiling'],
			['name' => 'Brick Laying'],
			['name' => 'Carpenter'],
			['name' => 'Carpet Fitter'],
			['name' => 'Electrician'],
			['name' => 'Heavy equipment operator'],
			['name' => 'Insulation installer'],
			['name' => 'Landscaper'],
			['name' => 'Painter'],
			['name' => 'Plumber'],
			['name' => 'Decorator']
		]);
    }
}
