<?php namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Repositories\UserRepository;

class UserRepositoryTest extends TestCase
{
	private $user;

	public function setUp(): void{
		parent::setUp();

		$this->user = new UserRepository;
		//this needs to be here to create all the db tables in memory before the tests are run
		\Artisan::call('migrate');
      	//\Artisan::call('db:seed');
	}
    /**
     * A basic test example.
     *
     * @return void
     */
 	public function testUserCreate()
 	{
 		$this->user->create([
 			'firstname' => 'test',
			'surname' => 'test',
 			'email' => 'test@test.com',
 			'password' => 'test123'
 		]);

		$users = $this->user->all();

		$this->assertCount(1, $users);
		//print_r(0)

		//$this->assertObjectHasAttribute($users);
 	}
}
